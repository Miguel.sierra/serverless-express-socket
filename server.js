//requires app
const express = require("express");
const app = express();
const logger = require("morgan");
const bodyparser = require("body-parser");
const helmet = require("helmet");
const dotenv = require("dotenv");
const serverless = require("serverless-http");

//initial configuration server
dotenv.config();
app.use(logger("dev"));
app.use(helmet());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

const port = process.env.PORT || 3000;

//Database
// const db = require("./models/index");
//Initial connection database
// db.sequelize
//   .authenticate()
//   .then(function() {
//     console.log("Connection has been established successfully.");
//   })
//   .catch(function(err) {
//     console.log("Unable to connect to the database:", err);
//   });

//Server routes
const membership = require("./routes/membership");

//Listen on port 3000
let server = app.listen(port, () => {
  console.log("Running on *:" + port);
});

let io = require("socket.io")(server);

//services

//socket
require("./services/socket")(io);

//middlewares
require("./middleware/socket")(app);

//Use server web routes
app.use("/service/", membership);

//for serverlesss deploy install serverless package npm and use
// serverless config credentials -p aws -k AKIAJLAZU2LECNEDJTOA -s IOtAwJqwUkEwvev74iUsqkLGFGe6xzf+SY/2xGqv
//serverless deploy
module.exports.handler = serverless(app);
