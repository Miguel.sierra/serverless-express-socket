//Server import
const express = require("express");
const membership = require("../controllers/membership");
const router = express.Router();

router.get("/membership", membership.store);

module.exports = router;
