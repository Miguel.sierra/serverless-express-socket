let socketService = io => {
  let users = 0;
  io.on("connection", function(socket) {
    console.log("a user connected");
    users += 1;
    console.log(users);

    socket.on("my other event", function(data) {
      console.log(data);
    });

    socket.on("disconnect", function() {
      users -= 1;
      console.log("user disconnected");
      console.log(users);
    });
  });
};

module.exports = socketService;
