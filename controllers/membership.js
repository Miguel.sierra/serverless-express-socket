// create some helper functions to work on the database

const store = async (req, res) => {
  try {
    req.io.emit("my other event", { my: "data" });
    req.io.disconnect();
    res.send("ok");
  } catch (error) {
    console.log(error);
    res.status(500).send("Error");
  }
};

module.exports = {
  store: store
};
