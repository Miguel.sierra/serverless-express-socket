let socket = app => {
  app.use(function(req, res, next) {
    let io = require("socket.io-client");
    var socket = io.connect("http://localhost:3000", {
      secure: true,
      rejectUnauthorized: false
    });
    req.io = socket;
    next();
  });
};

module.exports = socket;
