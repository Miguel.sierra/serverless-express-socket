module.exports = {
  dbConfig: {
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD
  }
};
